# Alitwitter

## Description

A simple twitter app that support CRUD operation for tweet only.

## Environment Setup

- Ruby 2.6.5
- Rails 6.0.2.1
- Bundler 2.1.4

After installing ruby, then you must first install rails with this
```
gem install rails -v 6.0.2.1
```

Then, install all depedency using bundle in root of directory project.
```
bundle install
```

## Run Test

Using rspec & rubocop
```
bundle exec rake
```

To open the coverage result in browser
```
bundle exec rake coverage
```

## How to Run

To run this application, you must run this first
```
yarn

rake db:migrate
```

and then run the rails app with
```
rails s
```

It will open the default twitter page directly.
